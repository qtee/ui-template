import { Injectable } from "@angular/core";
import { MatSnackBarRef, MatSnackBar } from "@angular/material/snack-bar";
import { SnackErrorComponent } from "src/app/services/common-services/ui/snack-error/snack-error.component";
import { StatusCaseMessage } from "../../authentication-services/bll/status-processor.service";
import { isNullOrUndefined } from "util";
import { SnackSuccessComponent } from "src/app/services/common-services/ui/snack-success/snack-success.component";

@Injectable({
  providedIn: "root"
})
export class UiSnackbarService {
  public currentErrorSnack: MatSnackBarRef<SnackErrorComponent>;
  public currentSuccessSnack: MatSnackBarRef<SnackSuccessComponent>;
  public snackBar: MatSnackBar;

  constructor(public newSnackBar: MatSnackBar) {
    this.snackBar = this.newSnackBar;
  }

  public OpenSnackBar(message: StatusCaseMessage, duration: number = 1500) {
    if (isNullOrUndefined(this.currentSuccessSnack) == false) {
      this.currentSuccessSnack.dismiss();
    }
    if (isNullOrUndefined(this.currentErrorSnack) == false) {
      this.currentErrorSnack.dismiss();
    }

    if (message.status) {
      this.currentSuccessSnack = this.snackBar.openFromComponent(
        SnackSuccessComponent,
        {
          data: message.text,
          duration: duration,
          panelClass: ["snack-result-success"]
        }
      );
    } else {
      this.currentErrorSnack = this.snackBar.openFromComponent(
        SnackErrorComponent,
        {
          data: message.text,
          duration: duration,
          panelClass: ["snack-result-error"]
        }
      );
    }
  }
}
