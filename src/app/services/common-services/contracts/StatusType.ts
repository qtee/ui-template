export enum StatusType {
  Disabled = 0,
  Moderating = 1,
  Enabled = 2,
  Blocked = 3,
  Deleted = 4
}
