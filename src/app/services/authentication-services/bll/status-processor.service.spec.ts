import { TestBed } from '@angular/core/testing';

import { StatusProcessorService } from './status-processor.service';

describe('StatusProcessorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatusProcessorService = TestBed.get(StatusProcessorService);
    expect(service).toBeTruthy();
  });
});
