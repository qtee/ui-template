import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor() { }

  public LogInfo(data: any) {
    if (!environment.production && environment.logLevel >= 1) {
      console.log(data);
    }
  }

  public LogWarning(data: any) {
    if (!environment.production && environment.logLevel >= 2) {
      console.log(data);
    }
  }

  public LogError(data: any) {
    if (!environment.production && environment.logLevel >= 3) {
      console.log(data);
    }
  }
}
