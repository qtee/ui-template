import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  constructor() { }


  public GetDate(date: any) {
    return moment.parseZone(date).utcOffset(moment().utcOffset()).format();
  }
  public GetDateFormated(date: any) {
    return moment.parseZone(date).format('DD.MM.YYYY');
  }

}
