import { createAction, props } from "@ngrx/store";
import { UiConfigurationModel } from "./common-services/contracts/UiConfigurationModel";

export const updateMobileNavigationMenuVisibleState = createAction(
  "[Storage uiConfigurationStore] MobileNavigationMenuVisible",
  props<UiConfigurationModel>()
);
