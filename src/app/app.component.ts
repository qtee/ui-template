import { Component, HostListener, OnInit, ViewChild } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Observable, Subscription } from "rxjs";
import { ConfigurationService } from "./services/configurationService";
import { LoggerService } from "./services/common-services/bll/extension/logger.service";
import { UiConfigurationModel } from "./services/common-services/contracts/UiConfigurationModel";
import { isNullOrUndefined } from "util";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.sass"]
})
export class AppComponent implements OnInit {
  constructor(
    private logger: LoggerService,
    private store: Store<{ count: any }>,
    public configurations: ConfigurationService
  ) {
    const _appComponent = this;
    this.uiConfigurationSubscription = configurations.uiConfigurationModel$.subscribe(
      {
        next(model) {
          if (
            !isNullOrUndefined(model) &&
            "MobileNavigationMenuVisible" in model
          ) {
            _appComponent.uiConfigurationModel = model;
          } else {
            _appComponent.uiConfigurationModel = new UiConfigurationModel();
          }
          _appComponent.checkNavigationState();
          logger.LogInfo(
            `Observe uiConfigurationSubscription in AppComponent: ${JSON.stringify(
              _appComponent.uiConfigurationModel
            )}`
          );
        },
        error(msg) {
          logger.LogError(`Error : ${JSON.stringify(msg)}`);
        }
      }
    );
  }

  uiConfigurationSubscription: Subscription;
  title = "qtee";
  uiConfigurationModel: UiConfigurationModel = new UiConfigurationModel();
  opened = false;
  hasMaxWidthNavigationBar = true;

  checkNavigationState() {
    this.hasMaxWidthNavigationBar =
      window.innerWidth <= this.configurations.MobileWithMax &&
      this.uiConfigurationModel.MobileNavigationMenuVisible;
    this.opened =
      window.innerWidth > this.configurations.MobileWithMax ||
      this.hasMaxWidthNavigationBar;
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.checkNavigationState();
  }

  ngOnInit() {
    this.checkNavigationState();
  }

  ngOnDestroy() {
    this.uiConfigurationSubscription.unsubscribe();
  }
}
