import { Injectable, ErrorHandler } from "@angular/core";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class GlobalErrorHandlerService implements ErrorHandler {
  constructor() {}
  handleError(error: any): void {
    if (!environment.production) console.log(error);
    // IMPORTANT: Rethrow the error otherwise it gets swallowed
    throw error;
  }
}
