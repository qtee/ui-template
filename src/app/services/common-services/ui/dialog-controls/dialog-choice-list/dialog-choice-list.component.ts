import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isNullOrUndefined } from 'util';
import { ChoiceListModel } from 'src/app/services/common-services/contracts/choiceListModel';

@Component({
  selector: 'app-dialog-choice-list',
  templateUrl: './dialog-choice-list.component.html',
  styleUrls: ['./dialog-choice-list.component.scss']
})
export class DialogChoiceListComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<DialogChoiceListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ChoiceListModel) {

    if (isNullOrUndefined(this.data)) {
      this.data = new ChoiceListModel(0, new Array());
    }
    this.isEnabled(null);
  }

  checkSum: number = 0;

  ngOnInit() {
  }
  closeDialog() {
    return this.dialogRef.close(this.data);
  }

  isEnabled(event) {
    this.checkSum = ChoiceListModel.GetSum(this.data);
  }
}