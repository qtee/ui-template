import { Injectable } from '@angular/core';
import { EndpointBaseService } from '../../common-services/dal/endpoint-base.service';
import { LoginModel } from 'src/app/services/user-profile-services/contracts/loginModel';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { ResultDataWrapper } from 'src/app/services/common-services/contracts/ResultWrapper';
import { UserProfileModel } from 'src/app/services/user-profile-services/contracts/userProfileModel';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginUserServiceService {

  private readonly _serviceUrl: string = `${environment.urlWebApi}/login`;
  constructor(private endpointBase: EndpointBaseService) { }

  public getToken(loginModel: LoginModel): Observable<HttpResponse<ResultDataWrapper<UserProfileModel>>> {
    return this.endpointBase.post(this._serviceUrl, loginModel);
  }
  public updateToken(): Observable<HttpResponse<ResultDataWrapper<UserProfileModel>>> {
    return this.endpointBase.post(`${this._serviceUrl + '/token'}`, {});
  }
}
