import { Injectable } from "@angular/core";
import { StorageService } from "../../common-services/bll/storage.service";
import { isNullOrUndefined } from "util";
import { UserStorageService } from "../../user-profile-services/bll/user-storage.service";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class TokenAuthStorageService {
  constructor(
    public storageService: StorageService,
    public userStorageService: UserStorageService
  ) {}

  public CheckTokenIsValid(tryUpdateToken: boolean): boolean {
    return true;
  }
}
