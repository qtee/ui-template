import { Injectable } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { isNullOrUndefined, isUndefined } from "util";
import { UserProfileModel } from "./user-profile-services/contracts/userProfileModel";
import { MatSnackBar, MatSnackBarRef } from "@angular/material/snack-bar";
import { SnackErrorComponent } from "./common-services/ui/snack-error/snack-error.component";
import { StatusCaseMessage } from "./authentication-services/bll/status-processor.service";
import { SnackSuccessComponent } from "./common-services/ui/snack-success/snack-success.component";
import { LoaderComponent } from "./common-services/ui/loader/loader.component";
import { LoaderService } from "./common-services/ui/loader.service";
import { UserProfileItem } from "./user-profile-services/contracts/userProfileItem";
import { StorageService } from "./common-services/bll/storage.service";
import { environment } from "src/environments/environment";
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";
import { UiConfigurationModel } from "./common-services/contracts/UiConfigurationModel";
import { updateMobileNavigationMenuVisibleState } from "./configurationService.actions";
import { LoggerService } from "./common-services/bll/extension/logger.service";

export interface IUserProfile {
  userProfile: UserProfileModel;
}
export interface IUserProfileCallback {
  UserProfileUpdated(): any;
}

@Injectable()
export class ConfigurationService {
  public readonly MobileWithMax = 940;
  public uiConfigurationModel$: Observable<UiConfigurationModel>;

  private static readonly STORAGE_LOGIN_REDIRECT_URL: string =
    "TOKEN_LOGIN_REDIRECT_URL";
  private static readonly TOKEN_EXP_TIME: number = 30;

  constructor(
    private logger: LoggerService,
    private loaderService: LoaderService,
    public storageService: StorageService,
    private uiConfigurationStorage: Store<{
      uiConfigurationModel: UiConfigurationModel;
    }>
  ) {
    this.storageService.SetData(
      ConfigurationService.STORAGE_LOGIN_REDIRECT_URL,
      ""
    );

    this.uiConfigurationModel$ = uiConfigurationStorage.pipe(
      select("uiConfigurationStore")
    );
  }

  public SetUiConfigurationModel(uiConfigurationModel: UiConfigurationModel) {
    this.uiConfigurationStorage.dispatch(
      updateMobileNavigationMenuVisibleState(uiConfigurationModel)
    );
    this.logger.LogInfo(
      `Run SetUiConfigurationModel: ${JSON.stringify(uiConfigurationModel)}`
    );
  }

  public GetLoginRedirectUrl() {
    return this.storageService.GetData(
      ConfigurationService.STORAGE_LOGIN_REDIRECT_URL
    );
  }

  public SetLoginRedirectUrl(loginRedirectUrl: string) {
    return this.storageService.SetData(
      ConfigurationService.STORAGE_LOGIN_REDIRECT_URL,
      loginRedirectUrl
    );
  }

  public GetApiUrl() {
    return environment.apiUrl;
  }

  private loaderRequests = 0;
  public LoaderOpen() {
    this.loaderRequests++;
    this.loaderService.toggle(true);
  }
  public LoaderClose() {
    this.loaderRequests--;
    if (this.loaderRequests == 0) {
      this.loaderService.toggle(false);
    }
  }
}
