import { Injectable } from "@angular/core";
import { EndpointBaseService } from "../../common-services/dal/endpoint-base.service";
import { Observable } from "rxjs";
import { HttpResponse } from "@angular/common/http";
import {
  ResultDataWrapper,
  ListWrapper,
  ResultWrapper
} from "src/app/services/common-services/contracts/resultWrapper";
import { FilterWrapper } from "src/app/services/common-services/contracts/filterWrapper";
import { KeyValueItem } from "src/app/services/common-services/contracts/keyValueItem";
import { environment } from "src/environments/environment";
import { ICrudApiClient } from "../../common-services/contracts/interfaces/icrud-api-client";

@Injectable({
  providedIn: "root"
})
export class ApiCustomerService implements ICrudApiClient {
  private readonly _serviceUrl: string = `${environment.urlProductApi}/supplier`;

  constructor(private endpointBase: EndpointBaseService) {}
  get<T>(id: string): Observable<HttpResponse<T>> {
    return this.endpointBase.get(`${this._serviceUrl}/${id}`);
  }
  filter<TFilter, TResponse>(
    filter: FilterWrapper<TFilter>
  ): Observable<HttpResponse<ListWrapper<TResponse>>> {
    return this.endpointBase.post(`${this._serviceUrl}/list`, filter);
  }
  create<T>(model: T): Observable<HttpResponse<ResultWrapper>> {
    return this.endpointBase.put(`${this._serviceUrl}`, model);
  }
  update<T>(model: T): Observable<HttpResponse<ResultWrapper>> {
    return this.endpointBase.post(`${this._serviceUrl}`, model);
  }
}
