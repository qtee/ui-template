import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ValidationService {
  constructor() {}
  public ValidateModel(controls: any, errorFields: any) {
    var keyNames = Object.keys(errorFields);
    for (let index = 0; index < keyNames.length; index++) {
      const element = keyNames[index];
      (controls[keyNames[index]] as any).setErrors(
        errorFields[keyNames[index]] as any
      );
      (controls[keyNames[index]] as any).markAsTouched();
    }
  }
}
