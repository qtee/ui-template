import { Injectable } from "@angular/core";

import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  CanLoad,
  Route
} from "@angular/router";

import { ConfigurationService } from "../../configurationService";

import { UserStorageService } from "../../user-profile-services/bll/user-storage.service";

import { TokenAuthStorageService } from "./token-auth-storage.service";

import { LoginUserServiceService } from "../dal/login-user-service.service";

import { isNullOrUndefined } from "util";

@Injectable()
export class AuthGuardService
  implements CanActivate, CanActivateChild, CanLoad {
  constructor(
    private configurationService: ConfigurationService,
    public userStorageService: UserStorageService,
    private tokenAuthStorageService: TokenAuthStorageService,
    private router: Router,
    private loginUserService: LoginUserServiceService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    let url: string = state.url;
    return this.checkLogin(url);
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.canActivate(route, state);
  }

  canLoad(route: Route): boolean {
    let url = `/${route.path}`;
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    if (this.tokenAuthStorageService.CheckTokenIsValid(true) === false) {
    }

    if (this.tokenAuthStorageService.CheckTokenIsValid(false) === true) {
      if (url === "/login") this.router.navigate(["/dashboard"]);

      if (!this.isAllowed(url)) this.router.navigate(["/dashboard"]);
      return true;
    }
    if (url !== "/login") {
      this.userStorageService.CleanUserProfileInfo();
      this.configurationService.SetLoginRedirectUrl(url);
      this.router.navigate(["/login"]);
    } else {
      return true;
    }

    return false;
  }

  isAllowed(url): boolean {
    var user = this.userStorageService.GetUserProfile(null, true);
    if (user.userProfileType != 1) return false;
    return true;
  }
}
