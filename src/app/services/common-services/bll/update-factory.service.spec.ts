import { TestBed } from '@angular/core/testing';

import { UpdateFactoryService } from './update-factory.service';

describe('UpdateFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateFactoryService = TestBed.get(UpdateFactoryService);
    expect(service).toBeTruthy();
  });
});
