import { Component, OnInit, HostListener, ViewChild } from "@angular/core";
import { MatAccordion } from "@angular/material/expansion";
import {
  ConfigurationService,
  IUserProfile
} from "src/app/services/configurationService";
import { UserProfileModel } from "src/app/services/user-profile-services/contracts/userProfileModel";
import { UserStorageService } from "src/app/services/user-profile-services/bll/user-storage.service";
import { isNullOrUndefined } from "util";
import { Subscribable, Subscription } from "rxjs";
import { LoggerService } from "../../bll/extension/logger.service";
import { UserRoleType } from "src/app/services/user-profile-services/contracts/roleType";
import { environment } from "src/environments/environment.prod";

@Component({
  selector: "app-navigation-bar",
  templateUrl: "./navigation-bar.component.html",
  styleUrls: ["./navigation-bar.component.sass"]
})
export class NavigationBarComponent implements OnInit, IUserProfile {
  constructor(
    private logger: LoggerService,
    public userStorageService: UserStorageService
  ) {
    let _navigationBarComponent = this;

    this.userProfileSubscription = userStorageService.userProfileModel$.subscribe(
      {
        next(model) {
          if (!isNullOrUndefined(model) && "login" in model) {
            _navigationBarComponent.userProfile = model;
          } else _navigationBarComponent.userProfile = null;
          logger.LogInfo(
            `Observe UserProfile in NavigationBarComponent: ${JSON.stringify(
              _navigationBarComponent.userProfile
            )}`
          );
        },
        error(msg) {
          logger.LogError(`Error : ${JSON.stringify(msg)}`);
        }
      }
    );

    if (isNullOrUndefined(_navigationBarComponent.userProfile) == true)
      _navigationBarComponent.userStorageService.GetUserProfile(this, true);
  }

  panelOpenState = true;
  userProfile: UserProfileModel = null;
  userProfileSubscription: Subscription;

  @ViewChild("matAccordion", { static: true }) Accordion: MatAccordion;

  ngOnInit() {
    // this.panelOpenState = true;
    // this.Accordion.openAll();
  }

  ngOnDestroy() {
    this.userProfileSubscription.unsubscribe();
  }

  isPanelHidden(): boolean {
    if (isNullOrUndefined(this.userProfile) == true) return true;
    return false;
  }
  isCustomer(): boolean {
    if (isNullOrUndefined(this.userProfile) == true) return false;
    return this.userProfile.userProfileType == UserRoleType.Customer;
  }
  openLogs() {
    if (environment.production == true) {
      window.open("http://localhost:5601/app/kibana", "_blank");
    } else {
      window.open("http://localhost:5601/app/kibana", "_blank");
    }
  }
}
