import { TestBed } from '@angular/core/testing';

import { EndpointBaseService } from './endpoint-base.service';

describe('EndpointBaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EndpointBaseService = TestBed.get(EndpointBaseService);
    expect(service).toBeTruthy();
  });
});
