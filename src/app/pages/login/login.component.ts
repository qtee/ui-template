import { Component, OnInit, HostListener } from "@angular/core";
import { Router } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { LoginUserServiceService } from "src/app/services/authentication-services/dal/login-user-service.service";
import { ApiUserProfileService } from "src/app/services/user-profile-services/dal/api-user-profile.service";
import { UserStorageService } from "src/app/services/user-profile-services/bll/user-storage.service";
import { UiSnackbarService } from "src/app/services/common-services/ui/ui-snackbar.service";
import { ConfigurationService } from "src/app/services/configurationService";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  StatusProcessorService,
  StatusCases
} from "src/app/services/authentication-services/bll/status-processor.service";
import { environment } from "src/environments/environment";
import { LoginModel } from "src/app/services/user-profile-services/contracts/loginModel";
import { isNullOrUndefined } from "util";

declare var grecaptcha: any;

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.sass"]
})
export class LoginComponent implements OnInit {
  constructor(
    private router: Router,
    private titleService: Title,
    private loginUserServiceService: LoginUserServiceService,
    private apiUserProfileService: ApiUserProfileService,
    public userStorageService: UserStorageService,
    public uiSnackbarService: UiSnackbarService,
    private configurations: ConfigurationService,
    private _formBuilder: FormBuilder,
    private statusProcessor: StatusProcessorService
  ) {
    if (environment.useHttpsRedirection && location.protocol === "http:") {
      window.location.href = location.href.replace("http", "https");
    }
    this.titleService.setTitle("Авторизация");
    this.loginModel = new LoginModel();
    this.clean();
  }

  location: Location;
  innerHeight: number = 0;

  formGroup: FormGroup;
  loginModel: LoginModel;
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

  ngOnInit() {
    this.innerHeight = window.innerHeight;
  }

  getResponceCapcha(captchaResponse: string) {
    this.verifyCaptcha(captchaResponse);
  }

  verifyCaptcha(captchaResponse: string) {
    console.log(captchaResponse);
  }

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.check();
    }
    if (event.keyCode == 27) {
      this.clean();
    }
  }

  private updateFormControls() {
    this.formGroup = this._formBuilder.group({
      login: [this.loginModel.login, Validators.required],
      password: [this.loginModel.password, Validators.required]
    });
  }

  clean() {
    this.loginModel.login = "";
    this.loginModel.password = "";
    this.updateFormControls();
  }
  check() {}
}
