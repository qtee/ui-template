
export class ResultWrapper {
    constructor(errorFields?: any, errorMessage?: string, userMessage?: string) {
        this.errorFields = errorFields;
        this.errorMessage = errorMessage;
        this.userMessage = userMessage;
    }

    public errorFields: any | null;
    public errorMessage: string | null;
    public userMessage: string | null;
}

export class ResultDataWrapper<T> {
    constructor(data?: T, errorFields?: any, errorMessage?: string, userMessage?: string) {
        this.data = data;
        this.errorFields = errorFields;
        this.errorMessage = errorMessage;
        this.userMessage = userMessage;
    }

    public data: T | null;
    public errorFields: any | null;
    public errorMessage: string | null;
    public userMessage: string | null;
}
export class ListWrapper<T> {
    public items: Array<T> | null;
    public itemsCount: number | null;
    public page: number | null;
}