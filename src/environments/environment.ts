export const environment = {
  logLevel: 1, // noLogs = 0, info = 1, warn = 2 , error = 3
  production: false,
  useHttpsRedirection: false,
  tokenExpirationCheckSeconds: 240,
  urlWebApi: '',
  urlMessageApi: '',
  urlProductApi: '',
  urlUserApi: '',
  apiUrl: ''
};