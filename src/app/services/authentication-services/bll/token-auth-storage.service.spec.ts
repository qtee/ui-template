import { TestBed } from '@angular/core/testing';

import { TokenAuthStorageService } from './token-auth-storage.service';

describe('TokenAuthStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TokenAuthStorageService = TestBed.get(TokenAuthStorageService);
    expect(service).toBeTruthy();
  });
});
