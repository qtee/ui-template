import {
  NgModule,
  LOCALE_ID,
  ErrorHandler,
  enableProdMode
} from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import {
  MatNativeDateModule,
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS
} from "@angular/material/core";
import { MatDialogModule } from "@angular/material/dialog";
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA
} from "@angular/material/snack-bar";
import { HttpClientModule } from "@angular/common/http";
import { MaterialModule } from "./material";

import { reducers } from "./reducers";

registerLocaleData(localeRU, "ru");

if (environment.production) {
  enableProdMode();
}

@NgModule({
  declarations: [
    AppComponent,
    ....
    SettingsComponent,
    FileUploaderComponent,
    DialogChoiceListComponent,
    TestComponent
    // AssortmentMarkerCreateComponent
  ],
  entryComponents: [
    DialogChoiceListComponent,
    FileUploaderComponent,
    AssortmentSpecificationsBatchUpdateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatDialogModule,
    EntityDataModule,
    EffectsModule.forRoot([AppEffects]),
    StoreModule.forRoot(reducers)
    // JoditAngularModule
  ],

  providers: [
    LoginUserServiceService,
    ConfigurationService,
    UserProfileBinderService,
    AuthGuardService,
    { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
    { provide: MatSnackBarRef, useValue: {} },
    { provide: MAT_SNACK_BAR_DATA, useValue: {} },
    { provide: LOCALE_ID, useValue: "ru" },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: "DD.MM.YYYY"
        },
        display: {
          dateInput: "DD.MM.YYYY",
          monthYearLabel: "MMM YYYY",
          dateA11yLabel: "LL",
          monthYearA11yLabel: "MMMM-YYYY"
        }
      }
    }
  ],
  bootstrap: [
    AppComponent,
    SnackErrorComponent,
    SnackSuccessComponent,
    LoaderComponent
  ]
})
export class AppModule {}
