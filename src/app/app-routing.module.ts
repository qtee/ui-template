import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { LoginComponent } from "./pages/login/login.component";
import { AuthGuardService } from "./services/authentication-services/bll/auth-guard.service";
import { UserProfilesComponent } from "./pages/user-profile/user-profiles/user-profiles.component";
import { UserProfileUpdateComponent } from "./pages/user-profile/user-profile-update/user-profile-update.component";
import { UserProfileCreateComponent } from "./pages/user-profile/user-profile-create/user-profile-create.component";
import { SupplierUpdateComponent } from "./services/supplier-services/ui/supplier-update/supplier-update.component";
import { SupplierCreateComponent } from "./services/supplier-services/ui/supplier-create/supplier-create.component";
import { SuppliersComponent } from "./services/supplier-services/ui/suppliers/suppliers.component";
import { TransactionCreateComponent } from "./pages/transaction-create/transaction-create.component";
import { TransactionUpdateComponent } from "./pages/transaction-update/transaction-update.component";
import { TestComponent } from "./test/ui/test/test.component";
import { SettingsComponent } from "./pages/settings/settings.component";
import { AssortmentVarietiesComponent } from "./services/assortment-variety-services/ui/assortment-varieties/assortment-varieties.component";
import { AssortmentVarietyCreateComponent } from "./services/assortment-variety-services/ui/assortment-variety-create/assortment-variety-create.component";
import { AssortmentVarietyUpdateComponent } from "./services/assortment-variety-services/ui/assortment-variety-update/assortment-variety-update.component";
import { AssortmentSpecificationCreateComponent } from "./services/assortment-specification-services/ui/assortment-specification-create/assortment-specification-create.component";
import { AssortmentSpecificationUpdateComponent } from "./services/assortment-specification-services/ui/assortment-specification-update/assortment-specification-update.component";
import { AssortmentSpecificationsComponent } from "./services/assortment-specification-services/ui/assortment-specifications/assortment-specifications.component";
import { SupplingSourcesComponent } from "./services/suppling-source-services/ui/suppling-sources/suppling-sources.component";
import { SupplingSourceCreateComponent } from "./services/suppling-source-services/ui/suppling-source-create/suppling-source-create.component";
import { SupplingSourceUpdateComponent } from "./services/suppling-source-services/ui/suppling-source-update/suppling-source-update.component";
import { AssortmentMarkersComponent } from './services/assortment-marker-services/ui/assortment-markers/assortment-markers.component';
import { AssortmentMarkerCreateComponent } from './services/assortment-marker-services/ui/assortment-marker-create/assortment-marker-create.component';
import { AssortmentMarkerUpdateComponent } from './services/assortment-marker-services/ui/assortment-marker-update/assortment-marker-update.component';
import { AssortmentsComponent } from './services/assortment-services/ui/assortments/assortments.component';
import { AssortmentUpdateComponent } from './services/assortment-services/ui/assortment-update/assortment-update.component';
import { AssortmentCreateComponent } from './services/assortment-services/ui/assortment-create/assortment-create.component';
import { AssortmentSpecificationsBatchUpdateComponent } from './services/assortment-specification-services/ui/assortment-specifications-batch-update/assortment-specifications-batch-update.component';

const routes: Routes = [
  { path: "login", component: LoginComponent, canActivate: [AuthGuardService] },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "settings",
    component: SettingsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "transaction-create/:accountId/:type",
    component: TransactionCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "transaction-update/:id",
    component: TransactionUpdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "user-profiles",
    component: UserProfilesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "user-profile-create",
    component: UserProfileCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "user-profile-update/:id",
    component: UserProfileUpdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "suppliers",
    component: SuppliersComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "supplier-create",
    component: SupplierCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "supplier-update/:id",
    component: SupplierUpdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "suppling-sources",
    component: SupplingSourcesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "suppling-source-create",
    component: SupplingSourceCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "suppling-source-update/:id",
    component: SupplingSourceUpdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortments",
    component: AssortmentsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-create",
    component: AssortmentCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-update/:id",
    component: AssortmentUpdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-varieties",
    component: AssortmentVarietiesComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-variety-create",
    component: AssortmentVarietyCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-variety-update/:id",
    component: AssortmentVarietyUpdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-specifications",
    component: AssortmentSpecificationsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-specification-create",
    component: AssortmentSpecificationCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-specification-update/:id",
    component: AssortmentSpecificationUpdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-markers",
    component: AssortmentMarkersComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-marker-create",
    component: AssortmentMarkerCreateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: "assortment-marker-update/:id",
    component: AssortmentMarkerUpdateComponent,
    canActivate: [AuthGuardService]
  },
  { path: "test", component: TestComponent },
  { path: "**", redirectTo: "/login" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
