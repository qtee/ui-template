import { userProfileReducer } from 'src/app/services/user-profile-services/bll/user-profile-storage.reducer';
import { uiConfigurationStoreReducer } from 'src/app/services/configurationService.reducer';
import { compose, combineReducers } from '@ngrx/store';

const reducers = {
    userProfileModel: userProfileReducer,
    uiConfigurationStore: uiConfigurationStoreReducer
};

const productionReducers = combineReducers(reducers);

export function reducer(state: any, action: any) {
    return productionReducers(state, action);
}