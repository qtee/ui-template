import { TestBed } from '@angular/core/testing';

import { LoginUserServiceService } from "./LoginUserServiceService";

describe('LoginUserServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginUserServiceService = TestBed.get(LoginUserServiceService);
    expect(service).toBeTruthy();
  });
});
