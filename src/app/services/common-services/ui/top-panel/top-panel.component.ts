import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigurationService, IUserProfile, IUserProfileCallback } from 'src/app/services/configurationService';
import { UserProfileModel } from 'src/app/services/user-profile-services/contracts/userProfileModel';
import { AccountSettingsType } from 'src/app/services/common-services/contracts/accountSettingsType';
import { isNullOrUndefined } from 'util';
import { UserStorageService } from 'src/app/services/user-profile-services/bll/user-storage.service';
import { LoggerService } from '../../bll/extension/logger.service';
import { UiConfigurationModel } from '../../contracts/UiConfigurationModel';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.sass']
})
export class TopPanelComponent implements OnInit, IUserProfile, IUserProfileCallback {

  constructor(private router: Router,
    public log: LoggerService,
    private logger: LoggerService,
    public userStorageService: UserStorageService,
    public configurations: ConfigurationService) {
    this.userProfile = this.userStorageService.GetUserProfile(this);
    this.UserProfileUpdated();


    const _appComponent = this;
    this.uiConfigurationSubscription = configurations.uiConfigurationModel$.subscribe({
      next(model) {
        if (!isNullOrUndefined(model) && 'MobileNavigationMenuVisible' in model) {
          _appComponent.uiConfigurationModel = model;
        } else {
          _appComponent.uiConfigurationModel = new UiConfigurationModel();
        }
        logger.LogInfo(`Observe uiConfigurationSubscription in TopPanelComponent: ${JSON.stringify(_appComponent.uiConfigurationModel)}`);
      },
      error(msg) { logger.LogError(`Error : ${JSON.stringify(msg)}`); }
    });

  }

  @Input('navigation-opened') navigationOpened: any;
  roleName: string;
  accountImage: string;
  userProfile: UserProfileModel;
  buttonMenuHidden = false;

  uiConfigurationSubscription: Subscription;
  uiConfigurationModel: UiConfigurationModel = new UiConfigurationModel();

  ngOnInit() {
    this.checkButtonMenuState();
  }

  ngOnDestroy() {
    this.uiConfigurationSubscription.unsubscribe();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkButtonMenuState();
  }

  checkButtonMenuState() {
    this.buttonMenuHidden = window.innerWidth > this.configurations.MobileWithMax;
  }


  navigationUpdate() {
    this.log.LogInfo(`navigationOpened ${this.navigationOpened}`);
    this.navigationOpened = !(this.navigationOpened == true);
    let uiConfigurationModel = new UiConfigurationModel();
    uiConfigurationModel.MobileNavigationMenuVisible = this.navigationOpened;
    this.configurations.SetUiConfigurationModel(uiConfigurationModel);
  }

  UserProfileUpdated() {

    this.userStorageService.GetAccountSettings();
    this.getRoleName();
    this.getImage();
  }

  logout() {
    this.userStorageService.CleanUserProfileInfo();
    this.router.navigate(['/login']);
  }

  getRoleName() {
    if (!isNullOrUndefined(this.userProfile))
      this.roleName = this.getRole(this.userProfile.userProfileType);
    else {
      this.roleName = "";
    }
  }
  getImage() {
    if (isNullOrUndefined(this.userProfile))
      this.accountImage = '';
    else
      this.accountImage = (this.userProfile.accountSettings == AccountSettingsType.Government ? "account_balance" : "account_balance_wallet");
  }
  setAccountSettingsToGv() {
    if (!isNullOrUndefined(this.userProfile)) {
      this.userStorageService.SetAccountSettings(AccountSettingsType.Government);
      this.router.navigate(['/login']);
    }
  }
  setAccountSettingsToOwn() {
    if (!isNullOrUndefined(this.userProfile)) {
      this.userStorageService.SetAccountSettings(AccountSettingsType.Own);
      this.router.navigate(['/login']);
    }
  }

  getRole(value) {
    if (value == 0)
      return 'Неизвестный';
    if (value == 1)
      return 'Администратор';
    if (value == 2)
      return 'Клиент';
    if (value == 3)
      return 'Модератор';
  }
}
