import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from "@ngrx/store";
import * as userProfileReducer  from '../services/user-profile-services/bll/user-profile-storage.reducer';
import * as uiConfigurationStoreReducer from '../services/configurationService.reducer';

export const reducers = {
  userProfileModel: userProfileReducer.reducer,
  uiConfigurationStore: uiConfigurationStoreReducer.reducer
};
