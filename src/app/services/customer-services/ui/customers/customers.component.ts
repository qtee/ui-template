import { Component, OnInit, HostListener, ViewChild } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { PageEvent, MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { FilterWrapper } from "src/app/services/common-services/contracts/filterWrapper";
import { ListWrapper } from "src/app/services/common-services/contracts/resultWrapper";
import { FormBuilder, FormGroup } from "@angular/forms";
import { isNullOrUndefined, log } from "util";
import { merge } from "rxjs";
import { startWith, switchMap, map, catchError } from "rxjs/operators";
import { ConfigurationService } from "src/app/services/configurationService";
import { AccountSettingsType } from "src/app/services/common-services/contracts/accountSettingsType";
import { UserStorageService } from "src/app/services/user-profile-services/bll/user-storage.service";
import { ApiCustomerService } from "src/app/services/customer-services/dal/api-customer.service";
import { LoggerService } from "src/app/services/common-services/bll/extension/logger.service";
import {
  CustomerItemFilterModel,
  CustomerItemModel
} from "../../contracts/customer-crud-models";

@Component({
  selector: "app-customers",
  templateUrl: "./customers.component.html",
  styleUrls: ["./customers.component.sass"]
})
export class CustomersComponent implements OnInit {
  title: string = "";
  panelOpenState = true;
  innerHeight: number = 0;
  filter: FilterWrapper<CustomerItemFilterModel>;
  formGroup: FormGroup;
  dataArray: ListWrapper<CustomerItemModel>;
  dataSource: MatTableDataSource<CustomerItemModel>;
  loginModel: any;
  constructor(
    private loggerService: LoggerService,
    private apiCustomerService: ApiCustomerService,
    public configurations: ConfigurationService,
    private router: Router,
    public userStorageService: UserStorageService,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private titleService: Title
  ) {
    this.title = "Поставщики";
    this.titleService.setTitle(this.title);

    this.dataArray = new ListWrapper<CustomerItemModel>();
    this.dataArray.items = new Array<CustomerItemModel>();

    this.dataSource = new MatTableDataSource(this.dataArray.items);

    this.filter = new FilterWrapper<CustomerItemFilterModel>();
    this.filter.filter = new CustomerItemFilterModel();
    this.filter.itemsPerPage = 50;
    this.filter.page = 1;
  }
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }
  onActivate(event) {}
  pageEvent: PageEvent;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  itemsCount = 0;
  pageSizeOptions = [1, 10, 20, 50, 100, 250];

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      console.log(event.keyCode);
      this.search();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.paginator._intl.itemsPerPageLabel = "На странице";

      this.paginator._intl.getRangeLabel = (
        page: number,
        pageSize: number,
        length: number
      ) => {
        if (length == 0 || pageSize == 0) {
          if (pageSize < length) return `${1 * pageSize} из ${length}`;
        } else if ((page + 1) * pageSize < length)
          return `${(page + 1) * pageSize} из ${length}`;
        else return `${length} из ${length}`;
      };

      this.paginator.pageSize = this.filter.itemsPerPage;

      this.paginator.page.subscribe(data => {
        this.filter.page = data.pageIndex + 1;
        this.filter.itemsPerPage = data.pageSize;
        this.executeQuery();
      });

      this.updateRoutParams();
      this.updateFormControls();
      this.executeQuery();
    });
  }

  ngOnInit() {
    this.innerHeight = window.innerHeight;
    this.updateFormControls();
  }

  private updateRoutParams() {
    if (!isNullOrUndefined(this.route.snapshot.queryParams.statuses))
      this.filter.filter.statuses = this.route.snapshot.queryParams.statuses;

    if (!isNullOrUndefined(this.route.snapshot.queryParams.nameKeyword))
      this.filter.filter.nameKeyword = decodeURIComponent(
        escape(window.atob(this.route.snapshot.queryParams.nameKeyword))
      );
    if (!isNullOrUndefined(this.route.snapshot.queryParams.pageSize))
      this.filter.itemsPerPage = Number.parseInt(
        this.route.snapshot.queryParams.pageSize
      );
    if (!isNullOrUndefined(this.route.snapshot.queryParams.pageIndex))
      this.filter.page = Number.parseInt(
        this.route.snapshot.queryParams.pageIndex
      );
  }
  private updateFormControls() {
    this.formGroup = this._formBuilder.group({
      status: [this.filter.filter.statuses],
      keyWord: [this.filter.filter.nameKeyword]
    });
  }

  executeQuery() {
    this.filter.orderDictionary = { Name: 0 };
    this.apiCustomerService
      .filter<CustomerItemFilterModel, CustomerItemModel>(this.filter)
      .subscribe(resp => {
        const keys = resp.headers.keys();
        if (resp.status != 200) {
          return;
        }
        if (resp.body) {
          this.loggerService.LogInfo(resp.body);
          this.dataArray = resp.body;
          this.filter.page = this.dataArray.page;
          this.itemsCount = this.dataArray.itemsCount;
          this.dataSource = new MatTableDataSource(this.dataArray.items);
        }
      });
  }

  search() {
    this.paginator.previousPage();

    this.filter.filter.nameKeyword = isNullOrUndefined(
      this.formGroup.value.keyWord
    )
      ? ""
      : this.formGroup.value.keyWord;
    this.filter.filter.statuses = [this.formGroup.value.status];
    this.filter.page = this.paginator.pageIndex + 1;
    this.filter.itemsPerPage = this.paginator.pageSize;
    var keyWord = window.btoa(
      unescape(encodeURIComponent(this.filter.filter.nameKeyword))
    );
    this.router.navigate(["/customers"], {
      queryParams: {
        statuses: this.filter.filter.statuses,
        nameKeyword: keyWord,
        pageSize: this.filter.itemsPerPage,
        pageIndex: this.filter.page
      }
    });
    this.executeQuery();
  }

  clean() {
    this.paginator.previousPage();
    this.filter.filter.nameKeyword = "";
    this.filter.filter.statuses = null;
    this.filter.page = 1;

    var keyWord = window.btoa(
      unescape(encodeURIComponent(this.filter.filter.nameKeyword))
    );

    this.updateFormControls();
    this.router.navigate(["/customers"], {
      queryParams: {
        statuses: this.filter.filter.statuses,
        nameKeyword: keyWord,
        pageSize: this.filter.itemsPerPage,
        pageIndex: this.filter.page
      }
    });
    this.executeQuery();
  }

  displayedColumns: string[] = ["name", "status", "actions"];
}
