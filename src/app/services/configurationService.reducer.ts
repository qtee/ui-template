import { createReducer, on, Action } from "@ngrx/store";
import { updateMobileNavigationMenuVisibleState } from "./configurationService.actions";
import { UiConfigurationModel } from "./common-services/contracts/UiConfigurationModel";

export const defaultState: UiConfigurationModel = new UiConfigurationModel();

export const uiConfigurationStoreReducer = createReducer(
  defaultState,
  on(updateMobileNavigationMenuVisibleState, (state, value) => {
    state = value;
    return state;
  })
);
export function reducer(
  state: UiConfigurationModel | undefined,
  action: Action
) {
  return uiConfigurationStoreReducer(state, action);
}
