import { Component, OnInit, HostListener } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { ApiCustomerService } from "src/app/services/customer-services/dal/api-customer.service";
import { CreateFactoryService } from "src/app/services/common-services/bll/create-factory.service";
import { KeyValueItem } from "src/app/services/common-services/contracts/keyValueItem";
import { StatusTypeListService } from "src/app/services/common-services/contracts/ui/StatusTypeListService";
import { LoggerService } from "src/app/services/common-services/bll/extension/logger.service";
import { CustomerModel } from '../../contracts/customer-crud-models';

@Component({
  selector: "app-customer-create",
  templateUrl: "./customer-create.component.html",
  styleUrls: ["./customer-create.component.sass"]
})
export class CustomerCreateComponent implements OnInit {
  title: string = "";
  innerHeight: number = 0;
  itemId: string;
  formGroup: FormGroup;
  currentModel: CustomerModel;
  statusList: KeyValueItem[];

  constructor(
    statusTypeListService: StatusTypeListService,
    private logger: LoggerService,
    private apiCustomerService: ApiCustomerService,
    private updateFactoryService: CreateFactoryService,
    private router: Router,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private titleService: Title
  ) {
    this.title = "Добавление подрядчика";
    this.titleService.setTitle(this.title);
    this.currentModel = new CustomerModel();
    this.statusList = statusTypeListService.GetList();
    this.updateFormControls();
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

  ngOnInit() {
    this.innerHeight = window.innerHeight;
  }

  private updateFormControls() {
    this.formGroup = this._formBuilder.group({
      name: [this.currentModel.name, Validators.required],
      status: [this.currentModel.status, Validators.required]
    });
  }

  @HostListener("window:resize", ["$event"])
  keyDownFunction(event) {
    if (event.keyCode === 13) {
      this.processAction();
    }
    if (event.keyCode === 27) {
      this.itemCancel();
    }
  }

  processAction() {
    this.updateFactoryService.process(
      this.apiCustomerService,
      this.formGroup,
      this.currentModel,
      () => {
        this.router.navigate(["/customers"]);
      }
    );
  }
  
  itemCancel() {
    this.router.navigate(["/customers"]);
  }
}
