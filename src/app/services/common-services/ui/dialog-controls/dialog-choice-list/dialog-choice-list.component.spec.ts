import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogChoiceListComponent } from './dialog-choice-list.component';

describe('DialogChoiceListComponent', () => {
  let component: DialogChoiceListComponent;
  let fixture: ComponentFixture<DialogChoiceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogChoiceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogChoiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
