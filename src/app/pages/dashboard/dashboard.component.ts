import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  panelOpenState = true;
  innerHeight: number = 0;
  constructor(private router: Router,
    private titleService: Title) {
    this.titleService.setTitle('Сводки');
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

  ngOnInit() {
    this.innerHeight = window.innerHeight;
  }
  @ViewChild('myaccordion', { static: false }) myPanels: MatAccordion;

  check() {
    this.router.navigate(['/purchase']);
  }
}
