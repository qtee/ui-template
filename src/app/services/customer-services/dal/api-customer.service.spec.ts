import { TestBed } from '@angular/core/testing';

import { ApiCustomerService } from './api-customer.service';

describe('CustomerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiCustomerService = TestBed.get(ApiCustomerService);
    expect(service).toBeTruthy();
  });
});
