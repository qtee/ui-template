import { Component, Inject } from "@angular/core";
import {
  MAT_SNACK_BAR_DATA,
  MatSnackBarRef
} from "@angular/material/snack-bar";

@Component({
  selector: "app-snack-error",
  templateUrl: "./snack-error.component.html",
  styleUrls: ["./snack-error.component.sass"]
})
export class SnackErrorComponent {
  constructor(
    public snackBarRef: MatSnackBarRef<SnackErrorComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any
  ) {}
  close() {
    this.snackBarRef.dismiss();
  }
}
