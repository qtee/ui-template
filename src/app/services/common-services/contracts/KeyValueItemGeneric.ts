export class KeyValueItem<K, V> {
  constructor(key: K = null, value: V = null) {
    this.key = key;
    this.value = value;
  }
  public key: K | null;
  public value: V | null;
}
