import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ConfigurationService } from '../../configurationService';
import { UiSnackbarService } from '../ui/ui-snackbar.service';
import { UserStorageService } from '../../user-profile-services/bll/user-storage.service';
import { StatusProcessorService, StatusCaseMessage } from '../../authentication-services/bll/status-processor.service';
import { HttpErrorResponse, HttpResponseBase, HttpResponse, HttpClient } from '@angular/common/http';
import { isNullOrUndefined } from 'util';
import { throwError, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LoggerService } from '../bll/extension/logger.service';

@Injectable({
  providedIn: 'root'
})
export class EndpointBaseService {

  constructor(private http: HttpClient, private router: Router,
    private logger: LoggerService,
    public configurations: ConfigurationService,
    public uiSnackbarService: UiSnackbarService,
    public userStorageService: UserStorageService,
    private statusProcessor: StatusProcessorService) {
  }
  public getServiceUrl(url: string) { return `${this.configurations.GetApiUrl()}${url}` }

  private handleError(error: HttpErrorResponse, currentContext: EndpointBaseService) {
    currentContext.configurations.LoaderClose();
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      this.logger.LogError(`An error occurred: ${error.error.message}`);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      this.logger.LogWarning(
        `Backend returned code ${error.status}, ` + `Body was: ${error.error}`);
    }
    var message = this.statusProcessor.GetMessageForResponse(error, null);
    if (!isNullOrUndefined(message)) {
      currentContext.uiSnackbarService.OpenSnackBar(message);
      if (error.status == 401 || error.status == 403)
        this.router.navigate(['/login']);
    }
    else {
      message = new StatusCaseMessage();
      message.text = error.status.toString();
    }

    return throwError(error);
  }
  private handleSuccess(httpResponse: HttpResponseBase, currentContext: EndpointBaseService) {

    currentContext.configurations.LoaderClose();
    var message = this.statusProcessor.GetMessageForResponse(httpResponse, null);
    if (!isNullOrUndefined(message))
      currentContext.uiSnackbarService.OpenSnackBar(message);

  }

  public getFile(serviceUrl: string): Observable<any> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen(); //application/octet-stream
    return this.http.get(this.getServiceUrl(serviceUrl), { responseType: 'blob', headers: headers, observe: 'response' }).pipe(
      //  retry(2), // retry a failed request up to 3 times
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )
    );
  }

  public postFile(serviceUrl: string, model: any): Observable<any> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen();
    return this.http.post(this.getServiceUrl(serviceUrl), model, { responseType: 'blob', headers: headers, observe: 'response' }).pipe(
      //  retry(2), // retry a failed request up to 3 times
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )
    );
  }

  public postForBlob(serviceUrl: string, model: any): Observable<any> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen();
    return this.http.post(this.getServiceUrl(serviceUrl), model, { responseType: 'blob', headers: headers, observe: 'response' }).pipe(
      //  retry(2), // retry a failed request up to 3 times
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )
    );
  }
  public postSimple(serviceUrl: string, model: any): Observable<any> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen();
    return this.http.post<any>(this.getServiceUrl(serviceUrl), model, { headers: headers, observe: 'response' }).pipe(
      //  retry(2), // retry a failed request up to 3 times
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )
    );
  }

  public post<TModelRequest, TModelResponse>(serviceUrl: string, model: TModelRequest): Observable<HttpResponse<TModelResponse>> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen();
    return this.http.post<TModelResponse>(this.getServiceUrl(serviceUrl), model, { headers: headers, observe: 'response' }).pipe(
      //  retry(2), // retry a failed request up to 3 times
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )
    );
  }
  public put<TModelRequest, TModelResponse>(serviceUrl: string, model: TModelRequest): Observable<HttpResponse<TModelResponse>> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen();
    return this.http.put<TModelResponse>(this.getServiceUrl(serviceUrl), model, { headers: headers, observe: 'response' }).pipe(
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )

      //  retry(2), // retry a failed request up to 3 times
      // then handle the error
    );
  }
  public get<TModelResponse>(serviceUrl: string): Observable<HttpResponse<TModelResponse>> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen();
    return this.http.get<TModelResponse>(this.getServiceUrl(serviceUrl), { headers: headers, observe: 'response' }).pipe(
      //  retry(2), // retry a failed request up to 3 times
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )
    );
  }
  public delete<TModelResponse>(serviceUrl: string): Observable<HttpResponse<TModelResponse>> {
    var headers = this.userStorageService.GetJsonAuthHeaders();
    this.configurations.LoaderOpen();
    return this.http.delete<TModelResponse>(this.getServiceUrl(serviceUrl), { headers: headers, observe: 'response' }).pipe(
      //  retry(2), // retry a failed request up to 3 times
      tap( // Log the result or error
        data => this.handleSuccess(data, this),
        error => this.handleError(error, this)
      )
    );
  }
}
