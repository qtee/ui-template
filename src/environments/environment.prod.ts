export const environment = {
  logLevel: 0, // noLogs = 0, info = 1, warn = 2 , error = 3
  production: true,
  useHttpsRedirection: true,
  tokenExpirationCheckSeconds: 240,
  urlWebApi: '',
  urlMessageApi: '',
  urlProductApi: '',
  urlUserApi: '',
  apiUrl: ''
};