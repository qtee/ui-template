import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private static readonly STORAGE_PURCHASE_SEARCH: string = "STORAGE_PURCHASE_SEARCH";

  public RemoveData(key: string) {
    localStorage.removeItem(key);
  }
  public SetData(key: string, data: any) {
    data = JSON.stringify(data);
    data = window.btoa(unescape(encodeURIComponent(data)));
    localStorage.setItem(key, data);
  }

  public GetData(key: string) {
    var value: any = localStorage.getItem(key);
    try {
      value = decodeURIComponent(escape(window.atob(value)));
      return JSON.parse(value);
    } catch (e) {
      if (value === 'undefined') {
        return void 0;
      }
    }
  }


  constructor() { }
}
