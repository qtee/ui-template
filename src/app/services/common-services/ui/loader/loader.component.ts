import { Component, OnInit, HostBinding } from "@angular/core";
import { LoaderService } from "src/app/services/common-services/ui/loader.service";

@Component({
  selector: "app-loader",
  templateUrl: "./loader.component.html",
  styleUrls: ["./loader.component.sass"]
})
export class LoaderComponent implements OnInit {
  @HostBinding("class.is-loader-open")
  isOpen = false;

  constructor(private loaderService: LoaderService) {}

  ngOnInit() {
    this.loaderService.change.subscribe(isOpen => {
      this.isOpen = isOpen;
    });
  }
}
