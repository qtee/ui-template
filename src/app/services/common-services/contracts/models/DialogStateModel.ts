export class DialogStateModel<DialogModelType, ResultModelType> {
  constructor(
    dialogModel: DialogModelType,
    resultModelType: ResultModelType = null,
    isValid: boolean = false
  ) {
    this.DialogModel = dialogModel;
    this.ResultModel = resultModelType;
    this.IsValid = isValid;
  }

  public DialogModel: DialogModelType;
  public ResultModel: ResultModelType;
  public IsValid: boolean;
}
