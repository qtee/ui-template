import { KeyValueItem } from "../KeyValueItem";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class StatusTypeListService {
  constructor() {}

  public GetList(): KeyValueItem[] {
    return [
      { key: 0, value: "Зарегистрирован" },
      { key: 2, value: "Активен" },
      { key: 3, value: "Заблокирован" },
      { key: 4, value: "Удален" }
    ];
  }
}
