import { Injectable, EventEmitter, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  isOpen = false;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  toggle(value: boolean) {
    this.isOpen = value;
    this.change.emit(this.isOpen);
  }
}
