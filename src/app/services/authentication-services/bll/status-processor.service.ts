import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpResponse, HttpResponseBase } from '@angular/common/http';

export enum StatusCases {
  Authorization,
  CreateItem,
  Read,
  UpdateItem,
  DeleteItem,
}
export class StatusCaseMessage {
  public text: string;
  public status: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class StatusProcessorService {

  private API_URL_EXCEPTION_LOGIN: string = "/login";
  private API_URL_EXCEPTION_TOKEN: string = "/login/token";

  constructor() { }

  public GetMessageForResponse<T>(response: HttpResponseBase | HttpErrorResponse, statusCase: StatusCases | null): StatusCaseMessage {
    if (response.status == 500 || response.status == 0)
      return { text: 'Произошла ошибка при обращении к серверу', status: false };

    if ((response.status == 401 || response.status == 403) &&
      !(response.url.includes(this.API_URL_EXCEPTION_LOGIN)
        || response.url.includes(this.API_URL_EXCEPTION_TOKEN)))
      return { text: 'Недостаточно прав для выполнения данного действия', status: false };



      if (statusCase == null && response.status == 400) 
      return { text: 'Данные неверны', status: false };

    if (statusCase == null && response.status == 404)
      return { text: 'Элемент отсутствует', status: false };


    if (statusCase == StatusCases.Authorization && response.status == 404)
      return { text: "Неверные авторизационные данные", status: false };

    if (statusCase == StatusCases.Authorization && response.status == 200)
      return { text: "Авторизация выполнена успешно", status: true };


    if (statusCase == StatusCases.CreateItem && response.status == 400)
      return { text: "Произошла ошибка при создании нового экземпляра", status: false };

    if (statusCase == StatusCases.CreateItem && response.status == 201)
      return { text: "Создании нового экземпляра прошло успешно", status: true };

      if (statusCase == null && response.status == 201)
        return { text: "Создании нового экземпляра прошло успешно", status: true };


    if (statusCase == StatusCases.Read && response.status == 400)
      return { text: "Произошла ошибка при получении данных", status: false };


    if (statusCase == StatusCases.UpdateItem && response.status == 400)
      return { text: "Произошла ошибка при обновлении данных", status: false };

    if (statusCase == StatusCases.UpdateItem && response.status == 202)
      return { text: "Обновление данных прошло успешно", status: true };
      
    if (statusCase == null && response.status == 202)
    return { text: "Обновление прошло успешно", status: true };


    if (statusCase == StatusCases.DeleteItem && response.status == 200)
      return { text: "Удаление прошло успешно", status: true };

    if (statusCase == StatusCases.DeleteItem && response.status == 400)
      return { text: "Произошла ошибка при удалении", status: false };

    return null;
  }

}
