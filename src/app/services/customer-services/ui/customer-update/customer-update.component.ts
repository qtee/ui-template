import { Component, OnInit, HostListener } from "@angular/core";
import { Router, ParamMap } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiCustomerService } from "src/app/services/customer-services/dal/api-customer.service";
import { UpdateFactoryService } from "src/app/services/common-services/bll/update-factory.service";
import { StatusTypeListService } from "src/app/services/common-services/contracts/ui/StatusTypeListService";
import { KeyValueItem } from "src/app/services/common-services/contracts/keyValueItem";
import { LoggerService } from "src/app/services/common-services/bll/extension/logger.service";
import { CustomerModel, CustomerUpdateModel } from '../../contracts/customer-crud-models';

@Component({
  selector: "app-customer-update",
  templateUrl: "./customer-update.component.html",
  styleUrls: ["./customer-update.component.sass"]
})
export class CustomerUpdateComponent implements OnInit {
  title: string = "";
  innerHeight: number = 0;
  itemId: string;
  formGroup: FormGroup;
  currentModel: CustomerModel;
  statusList: KeyValueItem[];

  constructor(
    statusTypeListService: StatusTypeListService,
    private logger: LoggerService,
    private apiCustomerService: ApiCustomerService,
    private updateFactoryService: UpdateFactoryService,
    private router: Router,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private titleService: Title
  ) {
    this.title = "Изменение подрядчика";
    this.titleService.setTitle(this.title);
    this.currentModel = new CustomerUpdateModel(null);
    this.statusList = statusTypeListService.GetList();
    this.updateFormControls();
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

  ngOnInit() {
    this.itemId = this.route.snapshot.paramMap.get("id");
    this.logger.LogInfo(this.itemId);
    this.innerHeight = window.innerHeight;

    this.apiCustomerService.get<CustomerModel>(this.itemId).subscribe(resp => {
      if (resp.status !== 200) {
        return;
      }

      if (resp.body) {
        this.currentModel = resp.body;
        this.updateFormControls();
      }
    });
  }

  private updateFormControls() {
    this.formGroup = this._formBuilder.group({
      name: [this.currentModel.name, Validators.required],
      status: [this.currentModel.status, Validators.required]
    });
  }

  @HostListener("window:resize", ["$event"])
  keyDownFunction(event) {
    if (event.keyCode === 13) {
      this.processAction();
    }
    if (event.keyCode === 27) {
      this.itemCancel();
    }
  }

  processAction() {
    this.updateFactoryService.process(
      this.apiCustomerService,
      this.formGroup,
      this.currentModel,
      () => {
        this.router.navigate(["/customers"]);
      }
    );
  }
  
  itemCancel() {
    this.router.navigate(["/customers"]);
  }
}
