import { TestBed } from '@angular/core/testing';

import { UiSnackbarService } from './ui-snackbar.service';

describe('UiSnackbarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UiSnackbarService = TestBed.get(UiSnackbarService);
    expect(service).toBeTruthy();
  });
});
