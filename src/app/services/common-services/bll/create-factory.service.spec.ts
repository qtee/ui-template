import { TestBed } from '@angular/core/testing';

import { CreateFactoryService } from './create-factory.service';

describe('CreateFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateFactoryService = TestBed.get(CreateFactoryService);
    expect(service).toBeTruthy();
  });
});
