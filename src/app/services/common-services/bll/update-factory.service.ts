import { Injectable } from "@angular/core";
import { ICrudApiClient } from "../contracts/interfaces/icrud-api-client";
import { FormGroup } from "@angular/forms";
import { ValidationService } from "./extension/validation.service";
import { LoggerService } from "./extension/logger.service";
import { environment } from "src/environments/environment";
import { isNullOrUndefined } from "util";

@Injectable({
  providedIn: "root"
})
export class UpdateFactoryService {
  constructor(
    private validationService: ValidationService,
    private loggerService: LoggerService
  ) {}

  public process(
    apiClient: ICrudApiClient,
    formGroup: FormGroup,
    model: any,
    successAction: any,
    errorAction: any = null
  ) {
    if (formGroup.valid == true) {
      for (let [key, value] of Object.entries(formGroup.value)) {
        this.loggerService.LogInfo(`key: ${key} , value: ${value}`);
        model[key] = value;
      }

      apiClient.update(model).subscribe(
        resp => {
          this.loggerService.LogInfo(
            `Update Request: ${model}; Update Response: ${resp}. `
          );
          successAction(resp);
        },
        error => {
          this.loggerService.LogError(
            `Update Request: ${model}; Update Error: ${error}. `
          );
          if (!isNullOrUndefined(error.error.errorFields)) {
            this.validationService.ValidateModel(
              formGroup.controls,
              error.error.errorFields
            );
          }
          if (errorAction != null) {
            errorAction(error);
          }
        }
      );
    } else {
      var fieldNames = Object.getOwnPropertyNames(formGroup.controls);
      for (let index = 0; index < fieldNames.length; index++) {
        (formGroup.controls[fieldNames[index]] as any).markAsTouched();
      }
    }
  }
}
