import { Observable } from 'rxjs/internal/Observable';
import { HttpResponse } from '@angular/common/http/http';
import { ResultWrapper, ListWrapper } from '../ResultWrapper';
import { FilterWrapper } from '../filterWrapper';

export interface ICrudApiClient {
  get<T>(id: string): Observable<HttpResponse<T>>;
  filter<TFilter, TResponse>(filter: FilterWrapper<TFilter>): Observable<HttpResponse<ListWrapper<TResponse>>>;
  create<T>(model: T): Observable<HttpResponse<ResultWrapper>>;
  update<T>(model: T): Observable<HttpResponse<ResultWrapper>>;
}
