
import { AccountSettingsType } from '../../common-services/contracts/accountSettingsType';
import { StatusType } from '../../common-services/contracts/statusType';

export class CustomerCreateModel {
    public name: string | null;
    public status: StatusType | null;
}
export class CustomerItemModel  {
    public id: string | null;
    public name: string | null;
    public status: StatusType | null;
}
export class CustomerItemFilterModel {
    public ids: string[];
    public nameKeyword: string;
    public statuses: StatusType[];
}
export class CustomerModel  {
    public id: string | null;
    public name: string | null;
    public status: StatusType | null;
}   
export class CustomerUpdateModel {
    constructor(customerModel: CustomerModel | null) {
        if (customerModel != null) {
            this.id = customerModel.id;
            this.name = customerModel.name;
            this.status = customerModel.status;
        }
    }
    public id: string;
    public name: string;
    public status: StatusType;
}