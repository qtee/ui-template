import { Component, Inject } from "@angular/core";
import {
  MAT_SNACK_BAR_DATA,
  MatSnackBarRef
} from "@angular/material/snack-bar";

@Component({
  selector: "app-snack-success",
  templateUrl: "./snack-success.component.html",
  styleUrls: ["./snack-success.component.sass"]
})
export class SnackSuccessComponent {
  constructor(
    public snackBarRef: MatSnackBarRef<SnackSuccessComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: any
  ) {}
  close() {
    this.snackBarRef.dismiss();
  }
}
